# Mask all live ebuilds so the user can cherry-pick which live ebuild he wants
# to install by unmasking it
#
# Please append to the top of this list as ebuilds are added


=gnome-extra/hamster-applet-9999
=x11-wm/metacity-9999
=net-misc/networkmanager-9999
=x11-themes/gtk-engines-murrine-9999
=dev-python/pygtk-9999
=dev-python/pygobject-9999
=app-crypt/seahorse-plugins-9999
=app-crypt/seahorse-9999
=dev-libs/libgweather-9999
=gnome-base/gnome-panel-9999
=x11-libs/pango-9999
=gnome-base/gdm-9999
=gnome-base/gconf-9999
=net-libs/libsoup-9999
=x11-libs/gtk+-2.9999
=dev-libs/glib-2.9999
=media-libs/swfdec-9999
