# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit gnome2-live

DESCRIPTION="Shared library to access the contents of an iPod"
HOMEPAGE="http://www.gtkpod.org/libgpod/"

EGIT_REPO_URI="git://gtkpod.git.sourceforge.net/gitroot/gtkpod/${PN}"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+gtk python +udev iphone"

RDEPEND=">=dev-libs/glib-2.16
	sys-apps/sg3_utils
	dev-libs/libxml2
	>=app-pda/libplist-1.0
	gtk? ( >=x11-libs/gtk+-2.6 )
	python? ( >=dev-lang/python-2.3
		>=media-libs/mutagen-1.8
		>=dev-python/pygobject-2.8 )
	udev? ( sys-fs/udev )"
DEPEND="${RDEPEND}
	python? ( >=dev-lang/swig-1.3.24 )
	iphone? ( app-pda/libimobiledevice )
	dev-util/pkgconfig
	dev-libs/libxslt"

pkg_setup() {
	# XXX: Add mono support
	G2CONF="${G2CONF}
		--without-hal
		--without-mono
		$(use_enable udev)
		$(use_enable gtk gdk-pixbuf)
		$(use_enable python pygobject)
		$(use_with python)
		$(use_with iphone libimobiledevice)"
	DOCS="README TROUBLESHOOTING AUTHORS NEWS README.SysInfo"
}
