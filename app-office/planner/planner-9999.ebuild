# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-office/planner/planner-0.14.4.ebuild,v 1.7 2010/10/06 20:35:22 eva Exp $

EAPI="3"
GCONF_DEBUG="no"

inherit autotools eutils git gnome2 python

DESCRIPTION="Project manager for Gnome2"
HOMEPAGE="http://live.gnome.org/Planner/"
SRC_URI=""
EGIT_REPO_URI="git://git.gnome.org/${PN}"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="alpha amd64 ppc sparc x86"
IUSE="doc eds libgda python examples"

RDEPEND=">=dev-libs/glib-2.6:2
	>=x11-libs/gtk+-2.6:2
	>=gnome-base/libgnomecanvas-2.10
	>=gnome-base/libgnomeui-2.10
	>=gnome-base/libglade-2.4
	>=gnome-base/gnome-vfs-2.10
	>=gnome-base/gconf-2.6
	>=dev-libs/libxml2-2.6.27
	>=dev-libs/libxslt-1.1.23
	libgda? ( gnome-extra/libgda:3 )
	python? ( >=dev-python/pygtk-2.6 )
	eds? (
		>=gnome-extra/evolution-data-server-1.1
		>=mail-client/evolution-2.1.3 )"

# FIXME: disable eds backend for now, its experimental

DEPEND="${RDEPEND}
	>=dev-util/pkgconfig-0.12
	app-text/scrollkeeper
	>=dev-util/intltool-0.35.5
	gnome-base/gnome-common
	>=dev-util/gtk-doc-am-1
	>=dev-util/gtk-doc-1"

DOCS="AUTHORS COPYING ChangeLog NEWS README"

pkg_setup() {
	G2CONF="${G2CONF}
		$(use_with libgda database gda3)
		$(use_enable python)
		$(use_enable python python-plugin)
		$(use_enable eds)
		--disable-update-mimedb"
#		$(use_enable eds eds-backend)
	python_set_active_version 2
}

src_unpack() {
	git_src_unpack
}

src_prepare() {
	gnome2_src_prepare

	# Fix compile failure with eds-plugin
	epatch "${FILESDIR}/${PN}-0.14.3-eds-plugin.patch"
	
	gtkdocize || die "gtkdocize failed"
	gnome-doc-common || die "gnome-doc-common failed"
	intltoolize --force --copy --automake || die "intltoolize failed"
	eautoreconf
}

src_install() {
	gnome2_src_install \
		sqldocdir="\$(datadir)/doc/${PF}" \
		sampledir="\$(datadir)/doc/${PF}/examples"

	if ! use examples; then
		rm -rf "${ED}/usr/share/doc/${PF}/examples"
	fi

	find "${ED}" -name "*.la" -delete || die "removal of *.la files failed"
}
