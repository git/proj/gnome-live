# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit autotools base git

DESCRIPTION="Parasite is a GTK+ debugger"
HOMEPAGE="http://chipx86.github.com/gtkparasite/"
EGIT_REPO_URI="git://github.com/chipx86/gtkparasite"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

# FIXME: python support is automagic
RDEPEND=">=x11-libs/gtk+-2.12.0
	>=dev-python/pygtk-2.10.3"
DEPEND="${RDEPEND}"

# All of these are currently empty
#DOCS="AUTHORS ChangeLog NEWS README"

src_unpack() {
	git_src_unpack

	cd "${S}"
	eautoreconf
}
